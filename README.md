# instacheck

Instagram username availability checker, written in 2019

## Overview
instacheck reads a list of line-separated usernames to check from a file, then checks to see if each one can be used. It does this by appending the username to the URL `https://instagram.com/`. In the event that this username is in use, the HTTP response will be the content of the user's Instagram profile, and thus an HTTP status code of `200` will be returned. For non-existent profiles, `404` is returned. We can use this information to easily determine whether or not a username is available, although this does not account for banned or deactivated accounts.