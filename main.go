// Copyright 2019 cpke
package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"sync"
)

// Read a line-separated list of Instagram usernames from a file, returning the results as a slice of strings
func readUsernamesFromFile(usernamesFile string) ([]string, error) {
	var usernames []string

	inFile, err := os.Open(usernamesFile)
	defer inFile.Close()

	if err != nil {
		return usernames, err
	}

	scanner := bufio.NewScanner(inFile)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		usernames = append(usernames, scanner.Text())
	}

	return usernames, nil
}

// checkUsernames takes a list of Instagram usernames, and checks if each one is available or taken.
// It will store each username and whether or not it is taken in a map, and return the result
func checkUsernames(usernames []string) map[string]bool {
	usernamesLength := len(usernames)
	results := make(map[string]bool, usernamesLength)

	var waitGroup sync.WaitGroup
	waitGroup.Add(usernamesLength)

	// Check each username in a new Go routine
	// Since we access one unique map key per routine, this is thread-safe
	for _, username := range usernames {
		go func(username string) {
			defer waitGroup.Done()
			status, _ := checkUsernameAvailable(username)
			results[username] = status
		}(username)
	}

	// Wait for all goroutines to finish
	waitGroup.Wait()

	return results
}

// Check if an Instagram username is available
// Returns a boolean indicating if the username is available (true) or taken (false), as well as any error encountered.
// The caller should always check if err != nil
func checkUsernameAvailable(username string) (bool, error) {
	url := "https://instagram.com/" + username
	resp, err := http.Get(url)
	defer resp.Body.Close() // ensure that the client response is closed no matter which return point

	if err != nil {
		return false, err
	}

	switch resp.StatusCode {
	case http.StatusOK:
		return false, nil
	case http.StatusNotFound:
		return true, nil
	default:
		return false, errors.New(fmt.Sprintf("received status code %d", resp.StatusCode))
	}
}

func main() {
	usernamesListFile := flag.String("list", "", "The filename containing the list of usernames to read from")
	debug := flag.Bool("debug", false, "Sets log level to debug")

	flag.Parse()

	// Default level for this example is info, unless debug flag is present
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if *debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	if *usernamesListFile == "" {
		flag.Usage()
		return
	}

	usernames, err := readUsernamesFromFile(*usernamesListFile)
	if err != nil {
		log.Error().
			Err(err).
			Msg("Error reading usernames from file")
	}

	results := checkUsernames(usernames)
	for username, status := range results {
		// No ternary operator in Go :(
		if status {
			log.Info().
				Str("username", username).
				Str("status", "available").
				Msg("")
		} else {
			log.Info().
				Str("username", username).
				Str("status", "taken").
				Msg("")
		}
	}
}
